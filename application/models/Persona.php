<?php
 class Persona extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funncion para insertar datos
public function insertar($datos){
  return $this->db -> insert("persona",$datos);
}
//funcion para actualizar deley
public function actualizar($id_per,$datos){
  $this->db->where('id_per',$id_per);
  return $this->db->update('persona',$datos);
}
//funcion para marcar el detalle de un cliente
public function consultarPorId($id_per){
  $this->db->where('id_per',$id_per);
  $this->db->join('genero','genero.id_gen=persona.fk_id_gen');
  $persona=$this->db->get("persona");
  if ($persona->num_rows()>0){
    //cuando hay clientes
    return $persona->row();
  }else{
    //cuando no hay clientes
    return false;

  }
 }




//funcion consultar todos los clientes
public function consultarTodos(){
  $this->db->join('genero','genero.id_gen=persona.fk_id_gen');
  $listadoPersonas=$this->db ->get("persona");
  if ($listadoPersonas->num_rows()>0){
    //cuando hay clientes
    return $listadoPersonas;
  }else{
    //cuando no hay clientes
    return false;

  }
 }

//eliminar cliente
 public function eliminar($id_per){
   $this->db->where("id_per",$id_per);
   return $this->db->delete("persona");

 }

 public function obtenerPorId($id_per){
   $this->db->where('id_per',$id_per);
   $query= $this->db->get('persona');
   if ($query->num_rows()>0) {
     // code...
     return $query->row();
   } else {
     return false;
   }
 }



}

 ?>
