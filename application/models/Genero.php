<?php
 class Genero extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funncion para insertar datos
public function insertar($datos){
  return $this->db -> insert("genero",$datos);
}
public function actualizar($id_gen,$datos){
  $this->db->where("id_gen",$id_gen);
  return $this->db->update("genero",$datos);
}
public function consultarPorId($id_gen){
  $this->db->where("id_gen",$id_gen);
  $genero=$this->db ->get('genero');
  if ($genero->num_rows()>0){
    //cuando hay clientes
    return $genero->row(); //cuando si hay
  }else{
    //cuando no hay clientes
    return false;//cuando no hay

  }
}
//funcion consultar todos los Alcancias
public function consultarTodos(){
  $listadoGeneros=$this->db ->get("genero");
  if ($listadoGeneros->num_rows()>0){
    //cuando hay Alcancias
    return $listadoGeneros;
  }else{
    //cuando no hay Alcancias
    return false;

  }
 }
//eliminar cliente
 public function eliminar($id_gen){
   $this->db->where("id_gen",$id_gen);
   return $this->db->delete("genero");

 }


}

 ?>
