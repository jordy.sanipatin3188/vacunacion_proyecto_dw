<?php
 class Dosi extends CI_Model{
  public function __construct(){
   parent::__construct();
}
//funncion para insertar datos
public function insertar($datos){
  return $this->db -> insert("dosis",$datos);
}
//funcion para actualizar deley
public function actualizar($id_dosis,$datos){
  $this->db->where('id_dosis',$id_dosis);
  return $this->db->update('dosis',$datos);
}
//funcion para marcar el detalle de un cliente
public function consultarPorId($id_dosis){
  $this->db->where('id_dosis',$id_dosis);
  $this->db->join('persona','persona.id_per=dosis.fk_id_per');
  $this->db->join('vacuna','vacuna.id_vac=dosis.fk_id_vac');
  $dosis=$this->db->get("dosis");
  if ($dosis->num_rows()>0){
    //cuando hay clientes
    return $dosis->row();
  }else{
    //cuando no hay clientes
    return false;

  }
 }




//funcion consultar todos los clientes
public function consultarTodos(){
  $this->db->join('persona','persona.id_per=dosis.fk_id_per');
  $this->db->join('vacuna','vacuna.id_vac=dosis.fk_id_vac');
  $listadoDosis=$this->db ->get("dosis");
  if ($listadoDosis->num_rows()>0){
    //cuando hay clientes
    return $listadoDosis;
  }else{
    //cuando no hay clientes
    return false;

  }
 }

//eliminar cliente
 public function eliminar($id_dosis){
   $this->db->where("id_dosis",$id_dosis);
   return $this->db->delete("cliente");


 }


 public function obtenerPorId($id_dosis){
   $this->db->where('id_dosis',$id_dosis);
   $query= $this->db->get('dosis');
   if ($query->num_rows()>0) {
     // code...
     return $query->row();
   } else {
     return false;
   }
 }



}

 ?>
