<br>
<h1> <center>Registro de Dosis</center> </h1>
<!--COLUMNA 1 enctype="multipart/form-data" agregar para qeu se suban los archivos-->
<form action="<?php echo site_url(); ?>/dosis/guardarDosis" method="post" id="frm_nuevo_cliente" enctype="multipart/form-data">

<b><label for="">PERSONA</label></b>
    <select class="form-control" name="fk_id_per" id="fk_id_per" required >
      <option value="">SELECCIONE UN PACIENTE</option>
      <?php if ($listadoPersonas): ?>
      <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
        <option value="<?php echo $personaTemporal->id_per; ?>">
          <?php echo $personaTemporal->nombre_per; ?>
        </option>
      <?php endforeach; ?>
    <?php endif; ?>

    </select>
    <br>
    <b><label for="">VACUNA</label></b>
    <select class="form-control" name="fk_id_vac" id="fk_id_vac" required >
      <option value="">SELECCIONE UN PAIS PORFAVOR</option>
      <?php if ($listadoVacunas): ?>
      <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
        <option value="<?php echo $vacunaTemporal->id_vac; ?>">
          <?php echo $vacunaTemporal->nombre_vac; ?>
        </option>
      <?php endforeach; ?>
    <?php endif; ?>

    </select>
    <br>
    <b>LUGAR: </b>
    <br>
    <input class="form-control" value="" type="number"  name="lugar_dosis" id="lugar_dosis"  placeholder="Por favor el centro de Vacunacion" class="form-control " required>
    <br>
    <b>NUMERO: </b>
    <br>
    <input type="text" class="form-control" value="" name="numero_dosis" id= "numero_dosis" placeholder="Ingrese el numero de dosis " class="form-control input-sm " required>
    <br>
    <button type="submit" name="button"  class="btn btn-primary">GUARDAR</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
    <button type="button" name="button"><a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-warning"><i class="fa solid fa-ban"></i> CANCELAR</a></button>

</form>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_cli:{
          letras:true,
          required:true
        },
        apellido_cli:{
          letras:true,
          required:true
        },
        telefono_cli:{
          required:true
        },
        email_cli:{
          email:true,
          required:true
        },
        direccion_cli:{
          required:true
        },
        estado_cli:{
          required:true
        }
      },
      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        identificacion_cli:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        nombre_cli:{
          required:"Porfavor ingrese su nombre",
          letras:"Porfavor no ingrese numeros",
        },
        apellido_cli:{
          required:"Porfavor ingrese su apellido",
          letras:"Porfavor no ingrese numeros",
        },
        telefono_cli:{
          required:"Porfavor ingrese su telefono",
          minlength:"El telefono debe tener mínimo 10 digitos",
          maxlength:"El telefono debe tener máximo 10 digitos",
          digits:"El telefono solo acepta números",
        },
        email_cli:{
          required:"Porfavor ingrese su email",
          email:"Email no valido utiliza un @ en el email precioso porfavor"
        },
        direccion_cli:{
          required:"Porfavor ingrese su direccion"
        },
        estado_cli:{
          required:"Porfavor ingrese su estado"
        }

      }
    });
</script>
<!--nueva importacion clase 29/6/2022-->
<script type="text/javascript">

  $('#foto_cli').fileinput({
    allowedFileExtensions:['jpeg','jpg','png'],
    dropZoneEnable:true,
    language:'es'

  });



</script>
