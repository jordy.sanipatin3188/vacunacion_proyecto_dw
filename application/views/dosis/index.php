<br>
<center>
  <h2>LISTADO DE DOSIS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/dosis/nuevo">Agregar Nuevo</a>
</center>

<?php if ($listadoDosis): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover" id="tbl-dosis">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE PACIENTE</th>
        <th class="text-center">VACUNA</th>
        <th class="text-center">LUGAR DE VACUNACION</th>
        <th class="text-center">NUMERO DE DOSIS</th>



      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoDosis->result() as $filaTemporal):  ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_dosis; ?>
          </td>
          <!--PARA PONER VISUALIZAR LAS IMAGENES EN EL FORMULARIO DEL INDEX-->

          <td class="text-center">
            <?php echo $filaTemporal->nombre_per; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_vac; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->lugar_dosis; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->numero_dosis; ?>
          </td>
          <td class="text-center">

            <a class="btn btn-success"  href="<?php echo site_url(); ?>/dosis/editar/<?php echo $filaTemporal->id_dosis; ?>" > <i class="fa fa-pen"></i></a>

          <a  href='javascript:void(0)'
          onclick="confirmarEliminacion('<?php echo$filaTemporal->id_dosis; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
          </a>
          </td>
        </tr>
  <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON DOSIS REGISTRADOS</h1>
  </div>
<?php endif; ?>

<script type="text/javascript">
  function confirmarEliminacion(id_dosis){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI BB</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/dosis/procesarEliminacion/"+ id_dosis;

        }, true],
        ['<button>NO BB</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>


<script type="text/javascript">
//YA ESTA EN ESPAÑOL
$(document).ready(function() {
    $("#tbl-dosis").DataTable( {
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-ES.json'
        }
    } );
} );



</script>
<script type="text/javascript">
    function confirmarEliminacion(id_dosis){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el dosis de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/dosis/procesarEliminacion/"+id_dosis;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
