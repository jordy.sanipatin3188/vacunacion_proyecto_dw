<br>
<h1> <center>Registro de Dosis</center> </h1>
<form  action="<?php echo site_url(); ?>/dosis/procesarActualizacion" method="post" enctype="multipart/form-data">

  <!--CAMBIAR ESTO PARA QUE APARESCA EL ID Y CAMBIAR ARRIBITA NOMAS -->
  <input type="hidden" name="id_dosis" id="id_dosis"value="<?php echo $dosis->id_dosis; ?>">


    <br>

    <b><label for="">PERSONA</label></b>
    <select class="form-control" name="fk_id_per" id="fk_id_per" required >
      <option value="">SELECCIONE UN PACIENTE</option>
      <?php if ($listadoPersonas): ?>
      <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
        <option value="<?php echo $personaTemporal->id_per; ?>">
          <?php echo $personaTemporal->nombre_per; ?>
        </option>
      <?php endforeach; ?>
    <?php endif; ?>

    </select>
    <br>
    <b><label for="">VACUNA</label></b>
    <select class="form-control" name="fk_id_vac" id="fk_id_vac" required >
      <option value="">SELECCIONE UN PAIS PORFAVOR</option>
      <?php if ($listadoVacunas): ?>
      <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
        <option value="<?php echo $vacunaTemporal->id_vac; ?>">
          <?php echo $vacunaTemporal->nombre_vac; ?>
        </option>
      <?php endforeach; ?>
    <?php endif; ?>

    </select>
    <br>
    <b>LUGAR: </b>
    <br>
    <input class="form-control" value="<?php echo $dosis->lugar_dosis; ?>" type="number"  name="lugar_dosis" id="lugar_dosis"  placeholder="Por favor el centro de Vacunacion" class="form-control " required>
    <br>
    <b>NUMERO: </b>
    <br>
    <input type="text" class="form-control" value='<?php echo $dosis->numero_dosis; ?>' name="numero_dosis" id= "numero_dosis" placeholder="Ingrese el numero de dosis " class="form-control input-sm " required>
    <br>

            <!--hasta aqui antes del boton guardar -->
    <br>
    <button type="submit" name="button"  class="btn btn-primary">GUARDAR</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
    <button type="button" name="button"><a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-warning">CANCELAR</a></button>

</form>


<script type="text/javascript">
//activando el pais seleccionado para el cliente
  $('#fk_id_pais').val('<?php echo $cliente->fk_id_pais; ?>');
  $('#estado_cli').val('<?php echo $cliente->estado_cli; ?>');
</script>

<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
      rules:{
        fk_id_per:{
          required:true
        },
        fk_id_vac:{
          required:true
        },
        lugar_dosis:{
          required:true
        },
        numero_dosis:{
          required:true
        }
      },
      messages:{
        fk_id_per:{
          required:"RELLENE ESTE CAMPO"
        },
        fk_id_vac:{
          required:"RELLENE ESTE CAMPO"
        },
        lugar_dosis:{
          required:"RELLENE ESTE CAMPO"
        },
        numero_dosis:{
          required:"RELLENE ESTE CAMPO"
        }
      }
    });
</script>

