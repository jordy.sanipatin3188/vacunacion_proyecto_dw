<div class="col-md-12">
        <br>
        <center>

            <h2><b> LISTADO DE GENEROS </b></h2>
            <br>
            <a href="<?php echo site_url(); ?>/generos/nuevo" class="btn btn-info">AGREGAR NUEVO GENERO</a>
        </center>
        </div>
</div>

    <br>
    <?php if ($listadoGeneros): ?>
    <table class="table table-bordered table-striped table-hover" id="tbl-generos">
      <thead>
        <tr>


            <th class="text-center">ID</th>
            <th class="text-center">NOMBRE DEL GENERO</th>


        </tr>

      </thead>
      <tbody>
        <?php foreach ($listadoGeneros->result() as $filaTemporal): ?>
          <tr>
                    <td class="text-center"> <?php echo $filaTemporal->id_gen; ?> </td>
                    <td class="text-center"> <?php echo $filaTemporal->nombre_gen; ?> </td>





<td class="text-center">

<a class="btn btn-info"  href="<?php echo site_url(); ?>/generos/editar/<?php echo $filaTemporal->id_gen; ?>" ><i class="fa fa-pen"></i></a>
<a  href='javascript:void(0)'
            onclick="confirmarEliminacion('<?php echo$filaTemporal->id_gen; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>
            </a>
</td>
</tr>
        <?php endforeach; ?>
      </tbody>
    </table>


    <?php else: ?>
      <div class="alert alert-danger">
        <h3>NO SE ENCONTRARON GENEROS REGISTRADOS</h3>
      </div>
    <?php endif; ?>
    <script type="text/javascript">
              function confirmarEliminacion(id_gen){
                iziToast.question({
                timeout: 20000,
                close: false,
                overlay: true,
                displayMode: 'once',
                id: 'question',
                zindex: 999,
                title: 'CONFIRMACION',
                message: 'ESTA SEGURO DE ELIMINAR ESTE GENERO DE FORMA PERMANENTE?',
                position: 'center',
                buttons: [
                    ['<button><b>SI</b></button>', function (instance, toast) {
             
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        window.location.href="<?php echo site_url(); ?>/generos/procesarEliminacion/"+id_gen;


             
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {
             
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
             
                    }],
                ]
            });

      }
    </script>
    </script>
  </div>

</div>

<script type="text/javascript">
          function confirmarEliminacion(id_gen){
            iziToast.question({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'CONFIRMACION',
            message: 'ESTA SEGURO DE ELIMINAR ESTA  DE FORMA PERMANENTE?',
            position: 'center',
            buttons: [
                ['<button><b>SI</b></button>', function (instance, toast) {
         
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                    window.location.href="<?php echo site_url(); ?>/generos/procesarEliminacion/"+id_gen;


         
                }, true],
                ['<button>NO</button>', function (instance, toast) {
         
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
         
                }],
            ]
        });

  }
</script>

  </script>


  <script type="text/javascript">
  $(document).ready( function () {
  	$('#tbl-generos').DataTable({
      dom: 'Bfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
      language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando START a END de TOTAL Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de MAX total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar MENU Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
          }
      },
    });
  } );
  </script>
