
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema de Vacunacion Covid-19</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
    <link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;700&amp;display=swap"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;700&amp;display=swap" media="print" onload="this.media='all'"/>
    <noscript>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;700&amp;display=swap"/>
    </noscript>
    <link href="<?php echo base_url();?>/assets/css/bootstrap.min.css?ver=1.2.0" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/css/font-awesome/css/all.min.css?ver=1.2.0" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/css/main.css?ver=1.2.0" rel="stylesheet">
  </head>
  <body id="top"><div class="site-wrapper">
  <div class="site-wrapper-inner">
    <div class="cover-container">
      <div class="masthead clearfix">
        <div class="inner">
          <h3 class="masthead-brand">Ministerio de Salud</h3>
          <nav class="nav nav-masthead">
            <a class="nav-link nav-social" href="https://www.facebook.com/SaludEcuador" title="Facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
            <a class="nav-link nav-social" href="https://twitter.com/Salud_Ec" title="Twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a>
            <a class="nav-link nav-social" href="https://www.youtube.com/user/EcuadorSalud" title="Youtube"><i class="fab fa-youtube" aria-hidden="true"></i></a>
            <a class="nav-link nav-social" href="https://www.instagram.com/minsaec/?hl=es" title="Instagram"><i class="fab fa-instagram" aria-hidden="true"></i></a>
          </nav>
        </div>
      </div>      
      <div class="inner cover">
        <h1 class="cover-heading">Sistema de Vacunacion COVID-19</h1>
        <p class="lead cover-copy">Cuida tu vida, cuida a tu familia, vacunate.</p>
        <p class="lead"><a type="button" class="btn btn-lg btn-default btn-notify" href="<?php echo site_url();?>/seguridades/formularioLogin">INGRESAR</a></p>
      </div>
      <div class="mastfoot">
        <div class="inner">
          <p>&copy; Ministerio de Salud Publica. Creado por: <a href="#" target="_blank">Jordy Sanipatin, Paul Topapanta</a>.</p>
        </div>
      </div>
    </div>
  </div>
</div>
    <script src="<?php echo base_url();?>/assets/scripts/jquery.slim.min.js?ver=1.2.0"></script>
    <script src="<?php echo base_url();?>/assets/scripts/bootstrap.bundle.min.js?ver=1.2.0"></script>
    <script src="<?php echo base_url();?>/assets/scripts/main.js?ver=1.2.0"></script>
  </body>
</html>