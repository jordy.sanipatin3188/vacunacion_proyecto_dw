<br>
<center>
  <h2>LISTADO DE PERSONAS</h2>
</center>
<hr>
<br>
<center>
  <a href="<?php echo site_url(); ?>/personas/nuevo">Agregar Nuevo</a>
</center>

<?php if ($listadoPersonas): ?>
  <!--TABLE-HOVER FUNCIONA PARAPONERUNA SOBRECUANDOPASA POR LA TABLA EL CURSOR-->
  <table class="table table-bordered table-striped table-hover" id="tbl-personas">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">IDENTIFICACION</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">DIRECCION</th>

        <th class="text-center">OPCIONES</th>


      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPersonas->result() as $filaTemporal):  ?>
        <tr>
          <td class="text-center">
            <?php echo $filaTemporal->id_per; ?>
          </td>
          <!--PARA PONER VISUALIZAR LAS IMAGENES EN EL FORMULARIO DEL INDEX-->
          <td class="text-center">
                  <?php if ($filaTemporal->foto_per!=""): ?>
                    <img src="<?php echo base_url(); ?>/uploads/personas/<?php echo $filaTemporal->foto_per; ?>"
                    height="80px"
                    width="100px"
                    alt="">
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
                </td>

          <td class="text-center">
            <?php echo $filaTemporal->cedula_per; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->apellido_per; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->nombre_per; ?>
          </td>

          <td class="text-center">
            <?php echo $filaTemporal->telefono_per; ?>
          </td>
          <td class="text-center">
            <?php echo $filaTemporal->email_per; ?>
          </td>

          <td class="text-center">

            <a class="btn btn-success"  href="<?php echo site_url(); ?>/personas/editar/<?php echo $filaTemporal->id_per; ?>" > <i class="fa fa-pen"></i></a>



            <!--<a  href='javascript:void(0)'
            onclick="confirmarEliminacion('<?php echo$filaTemporal->id_per; ?>');"
              class="btn btn-danger">
              <i class="fa fa-trash"></i>-->
            </a>

          <a  href='javascript:void(0)'
          onclick="confirmarEliminacion('<?php echo$filaTemporal->id_per; ?>');"
            class="btn btn-danger">
            <i class="fa fa-trash"></i>
          </a>

          </td>
        </tr>
  <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <div class="alert alert-danger">
    <h1>NO SE ENCONTRARON PERSONAS REGISTRADOS</h1>
  </div>
<?php endif; ?>

<script type="text/javascript">
  function confirmarEliminacion(id_per){
    iziToast.question({
    timeout: 10000,
    close: false,
    overlay: true,
    displayMode: 'once',
    id: 'question',
    zindex: 999,
    title: 'CONFIRMACION',
    message: 'ESTAS SEGURO DE ELIMINAR',
    position: 'center',
    buttons: [
        ['<button><b>SI BB</b></button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
            window.location.href="<?php echo site_url(); ?>/personass/procesarEliminacion/"+ id_per;

        }, true],
        ['<button>NO BB</button>', function (instance, toast) {

            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

        }],
    ]
});

  }

</script>


<script type="text/javascript">
//YA ESTA EN ESPAÑOL
$(document).ready(function() {
    $("#tbl-personas").DataTable( {
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-ES.json'
        }
    } );
} );



</script>
<script type="text/javascript">
    function confirmarEliminacion(){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar la persona de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/personas/procesarEliminacion/";

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
