<br>
<h1> <center>AGREGAR NUEVA PERSONA</center> </h1>
<form  action="<?php echo site_url(); ?>/personas/guardarPersona" method="post" enctype="multipart/form-data">



    <br>
    <b>IDENTIFICACION: </b>
    <br>
    <input class="form-control" value="" type="number"  name="cedula_per" id="cedula_per"  placeholder="Por favor ingrese la identificacion" class="form-control " required>
    <br>
    <b>NOMBRE: </b>
    <br>
    <input type="text" class="form-control" value='' name="nombre_per" id= "nombre_per" placeholder="Ingrese su nombre" class="form-control input-sm " required>
    <br>
    <b>APELLIDO: </b>
    <br>
    <input type="text" class="form-control" value="" name="apellido_per" id= "apellido_per"value="" placeholder="Ingrese el apellido" class="form-control input-sm " required>
    <br>
    <b>TELEFONO: </b>
    <br>
    <input type="number" class="form-control" value="" name="telefono_per" id= "telefono_per" placeholder="Ingrese su numero de telefono" class="form-control input-sm " required>
    <br>
    <b>EMAIL: </b>
    <br>
    <input type="email" class="form-control" value="" name="email_per" id= "email_per" placeholder="Ingrese su email" class="form-control input-sm " required>
    <br>

            <!--para crear y poner una foto accept para que unicamente seleccione imagenes-->
            <br>
            <br>
            <b>FOTOGRAFIA: </b>
            <input type="file" class="form-control" value="" name="foto_per" id='foto_per' class="form-control input-sm " accept="image/*"  >

            <br>
            <br>
            <!--hasta aqui antes del boton guardar -->
    <br>
    <button type="submit" name="button"  class="btn btn-primary">GUARDAR</a></button>
    <!--PARA DAR ESPACICOS HACIA LA DERECHA-->
    &nbsp;&nbsp;&nbsp;
    <button type="button" name="button"><a href="<?php echo site_url(); ?>/personas/index" class="btn btn-warning">CANCELAR</a></button>

</form>




<script type="text/javascript">
    $("#frm_nuevo_persona").validate({
      rules:{
        fk_id_pais:{
          required:true
        },
        identificacion_per:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true
        },
        nombre_per:{
          letras:true,
          required:true
        },
        apellido_per:{
          letras:true,
          required:true
        },
        telefono_per:{
          required:true
        },
        email_per:{
          email:true,
          required:true
        },
        direccion_per:{
          required:true
        },
        estado_per:{
          required:true
        }
      },
      messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
        identificacion_per:{
          required:"Por favor ingrese el número de cédula",
          minlength:"La cédula debe tener mínimo 10 digitos",
          maxlength:"La cédula debe tener máximo 10 digitos",
          digits:"La cédula solo acepta números"
        },
        nombre_per:{
          required:"Porfavor ingrese su nombre",
          letras:"Porfavor no ingrese numeros",
        },
        apellido_per:{
          required:"Porfavor ingrese su apellido",
          letras:"Porfavor no ingrese numeros",
        },
        telefono_per:{
          required:"Porfavor ingrese su telefono",
          minlength:"El telefono debe tener mínimo 10 digitos",
          maxlength:"El telefono debe tener máximo 10 digitos",
          digits:"El telefono solo acepta números",
        },
        email_per:{
          required:"Porfavor ingrese su email",
          email:"Email no valido utiliza un @ en el email precioso porfavor"
        },
        direccion_per:{
          required:"Porfavor ingrese su direccion"
        },
        estado_per:{
          required:"Porfavor ingrese su estado"
        }

      }
    });
</script>

<script type="text/javascript">

  $('#foto_per').fileinput({
    allowedFileExtensions:['jpeg','jpg','png'],
    dropZoneEnable:true,
    language:'es'

  });



</script>
