<?php
    class Dosis extends CI_Controller{
      public function __construct(){
        parent::__construct();
        //MODELO CLIENTE
        $this->load->model("dosi");
        $this->load->model("vacuna");
        $this->load->model("persona");
        //validando si alguien esta conectado
        if ($this->session->userdata('c0nectadoUTC')) {
          //SI ESTA CONECTADO
          // code...
        } else {
          redirect('seguridades/formularioLogin');
        }

      }
      public function index(){
        $data["listadoDosis"]=$this->dosi->consultarTodos();
         $this->load->view("header");
         $this->load->view("dosis/index",$data);
         $this->load->view("footer");

      }
      public function nuevo(){
        $data ['listadoVacunas']=$this->vacuna->obtenerTodos();
        $data ['listadoPersonas']=$this->persona->consultarTodos();
        $this->load->view("header");
        $this->load->view("dosis/nuevo",$data);
        $this->load->view("footer");
      }

      public function editar($id_dosis){
        $data ['listadoVacunas']=$this->vacuna->consultarTodos();
        $data ['listadoPersonas']=$this->persona->consultarTodos();
        $data['dosis']=$this->dosi->consultarPorId($id_dosis);
        $this->load->view("header");
        $this->load->view("dosis/editar",$data);
        $this->load->view("footer");
      }
//para actualizar ahorita clase del jueves

public function procesarActualizacion(){
$id_cli = $this->input->post("id_dosis");
$datosClienteEditado=array(
"lugar_dosis"=>$this->input->post("lugar_dosis"),
"numero_dosis"=>$this->input->post("numero_dosis"),
"fk_id_vac"=>$this->input->post("fk_id_per"),
"fk_id_per"=>$this->input->post("fk_id_vac")


);


if ($this->dosi->actualizar($id_dosis,$datosDosisEditado)) {

            //Pongan esto para que al momento de editar salga el mensaje flash
            $this->session->set_flashdata('confirmacion','CLIENTE EDITADO EXITOSAMENTE');
          } else {
            $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
          }
          redirect("dosis/index");
          }

//guardar cliente en el controlador clientes - agregar un
//dato en el controlador para que se guarde los archivos
      public function guardarDosis(){
        $datosNuevoDosis=array(
          "lugar_dosis"=>$this->input->post("lugar_dosis"),
          "numero_dosis"=>$this->input->post("numero_dosis"),
          "fk_id_vac"=>$this->input->post("fk_id_per"),
          "fk_id_per"=>$this->input->post("fk_id_vac")
           
        );


        if($this->dosi->insertar($datosNuevoDosis)){
          //echo "INSERCION EXITOSA";
          //crear sesion flash
          $this->session->set_flashdata('confirmacion','Dosis insertado exitosamente');
        }else{
          $this->session->set_flashdata('error','Error al procesar, intente nuevamente');
        }
        redirect("dosis/index");
      }

      public function procesarEliminacion($id_cli){

        $data['dosis']=$this->dosis->obtenerPorId($id_dosis);
        if($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR"){
            if($this->cliente->eliminar($id_dosis)){
                redirect("dosis/index");
      }
          else{
                echo "ERROR AL ELIMINAR";
              }
        }
      }
    }//cierre de la clase


 ?>
